php-horde-text-diff (2.2.1-3) unstable; urgency=medium

  * d/watch: Switch to format version 4.
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.
  * debian/patches: Add 1012_php8.2.patch. Fix PHP 8.2 deprecation warnings.
    (Closes: #1028947).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 25 Jan 2023 22:15:03 +0100

php-horde-text-diff (2.2.1-2) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 14 Sep 2020 21:44:45 +0200

php-horde-text-diff (2.2.1-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * New upstream version 2.2.1
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.
  * d/copyright: Fix path for unit test files.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 11:16:15 +0200

php-horde-text-diff (2.2.0-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959310).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 07 May 2020 10:31:22 +0200

php-horde-text-diff (2.2.0-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:16:02 +0200

php-horde-text-diff (2.2.0-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 14:36:09 +0200

php-horde-text-diff (2.2.0-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 21:50:42 +0200

php-horde-text-diff (2.2.0-1) unstable; urgency=medium

  * New upstream version 2.2.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Jul 2017 21:54:49 +0200

php-horde-text-diff (2.1.2-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 08:38:37 +0200

php-horde-text-diff (2.1.2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 16:53:05 +0100

php-horde-text-diff (2.1.2-1) unstable; urgency=medium

  * New upstream version 2.1.2

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 23:19:32 +0100

php-horde-text-diff (2.1.1-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Fri, 23 Oct 2015 07:33:01 +0200

php-horde-text-diff (2.1.1-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 00:58:31 +0200

php-horde-text-diff (2.1.1-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.1.1

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 22:20:18 +0200

php-horde-text-diff (2.1.0-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 14:36:41 +0200

php-horde-text-diff (2.1.0-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 15:10:26 +0200

php-horde-text-diff (2.1.0-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 23:05:45 +0200

php-horde-text-diff (2.1.0-1) unstable; urgency=medium

  * New upstream version 2.1.0

 -- Mathieu Parent <sathieu@debian.org>  Fri, 20 Jun 2014 07:58:08 +0200

php-horde-text-diff (2.0.2-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:33:51 +0200

php-horde-text-diff (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 16:09:47 +0200

php-horde-text-diff (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:43:57 +0100

php-horde-text-diff (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Dec 2012 11:24:23 +0100

php-horde-text-diff (1.0.2-1) unstable; urgency=low

  * Horde_Text_Diff package
  * Initial packaging (Closes: #657369)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Sun, 12 Feb 2012 15:00:37 +0100
